package ken47.temp.droidbase2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import ken47.temp.droidbase2.abstrakt.AuthorizedActivity;
import ken47.temp.droidbase2.helpers.CallableTask;
import ken47.temp.droidbase2.helpers.GiftApi;
import ken47.temp.droidbase2.helpers.SecuredRestBuilder;
import ken47.temp.droidbase2.helpers.TaskCallback;
import ken47.temp.droidbase2.helpers.UserState;
import ken47.temp.droidbase2.models.Gift;

public class GiftListActivity extends AuthorizedActivity {
    private SharedPreferences sharedPrefs;
    private EditText mSearchInputView;
    private Button mSearchButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setLayoutId(R.layout.gift_list_activity);
        super.onCreate(savedInstanceState);
        sharedPrefs = getApplicationContext().getSharedPreferences(AuthorizedActivity.APP_SHARED_PREFS, Context.MODE_PRIVATE);
        setViews();
        loadGifts();
    }

    private void setViews() {
        mSearchInputView = (EditText)findViewById(R.id.searchInputView);
        mSearchButton = (Button)findViewById(R.id.searchButton);
    }

    private void displayGifts(List<Gift> gifts) {
        // We get the ListView component from the layout
        ListView lv = (ListView) findViewById(R.id.listView);
        final List<Map<String, String>> sourceList = new ArrayList<Map<String, String>>();

        for(Gift gift : gifts) {
            HashMap<String, String> tmp = new HashMap<String, String>();
            if (sharedPrefs.getBoolean(UserState.SHOULD_HIDE_FLAGGED, false) && gift.getFlaggedCount() > 0) {
                Log.d(GiftListActivity.class.getCanonicalName(), "Flagged gift hidden, id: " + ("" + gift.getId()));
                continue;
            }
            tmp.put("id", "" + gift.getId());
            tmp.put("title", gift.getTitle());
            StringBuilder sb = new StringBuilder();
            sb.append(gift.getUsername());
            String giftChainName = gift.getGiftChainName();
            if (null != giftChainName && !giftChainName.isEmpty()) {
                sb.append(" / " + gift.getGiftChainName());
            }
            tmp.put("usernameAndGiftChain", sb.toString());
            sourceList.add(tmp);
        }

        // This is a simple adapter that accepts as parameter
        // Context
        // Data list
        // The row layout that is used during the row creation
        // The keys used to retrieve the data
        // The View id used to show the data. The key number and the view id must match
        SimpleAdapter simpleAdapter = new SimpleAdapter(this, sourceList, R.layout.gift_list_item_view, new String[] {"title", "usernameAndGiftChain"}, new int[] {R.id.titleText, R.id.usernameText});
        lv.setAdapter(simpleAdapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parentAdapter, View view, int position, long id) {

                // We know the View is a TextView so we can cast it
                Map<String, String> item = sourceList.get(position);
                Intent intent = new Intent(GiftListActivity.this, SingleGiftActivity.class);
                Bundle b = new Bundle();
                b.putLong(SingleGiftActivity.GIFT_ID, Long.parseLong(item.get("id"))); //Your id
                intent.putExtras(b);
                startActivity(intent);
            }
        });
    }

    private boolean loadMine = false;
    public void loadMyGifts(View view) {
        loadMine = true;
        loadGifts();
    }

    public void loadGifts(View view) {
        loadGifts();
    }

    private void loadGifts() {
        mSearchButton.setEnabled(false);

        CallableTask.invoke(new Callable<List<Gift>>() {
            @Override
            public List<Gift> call() throws Exception {
                String searchTerm = mSearchInputView.getText().toString();
                GiftApi api = SecuredRestBuilder.getRestAdapter(sharedPrefs).create(GiftApi.class);

                if (loadMine) {
                    return api.loadGiftsByUser(sharedPrefs.getString(UserState.USERNAME, ""));
                } else if (searchTerm != null && !searchTerm.isEmpty()) {
                    return api.searchGiftsByTitle(searchTerm);
                } else {
                    return api.getMostRecentGifts();
                }
            }
        }, new TaskCallback<List<Gift>>()  {

            @Override
            public void success (List<Gift> gifts) {
                displayGifts(gifts);
            }

            @Override
            public void error (Exception e){
                Log.e(LoginActivity.class.getName(), "Error loading user info.", e);

                Toast.makeText(
                    GiftListActivity.this,
                    "Load error! Try again in a minute.",
                    Toast.LENGTH_SHORT).show();
            }

            @Override
            public void always () {
                loadMine = false;
                mSearchButton.setEnabled(true);
            }
        });

    }
}
