package ken47.temp.droidbase2.helpers;

import android.content.SharedPreferences;

/**
 * Created by kenster on 11/29/14.
 */
public class UserState {
    public static final String OAUTH_TOKEN = "oauthToken";
    public static final String OAUTH_TOKEN_EXPIRES_AT = "oauthTokenExpiresAt";

    public static final String USERNAME = "username";
    public static final String EMAIL = "user_email";
    public static final String UPDATE_INTERVAL = "update_interval";
    public static final String SHOULD_HIDE_FLAGGED = "should_hide_flagged";

    public static boolean isProbablyLoggedIn(SharedPreferences sharedPreferences) {
        Long expiresAt = sharedPreferences.getLong(OAUTH_TOKEN_EXPIRES_AT, 0);
        String token = sharedPreferences.getString(OAUTH_TOKEN, "");
        Long now = System.currentTimeMillis() / 1000L;
        return now < expiresAt;
    }
}
