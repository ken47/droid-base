package ken47.temp.droidbase2.models;

import com.google.common.base.Objects;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * This class represents a Category of a Video. Each Video can
 * be in exactly one Category (e.g., romance, action, horror, etc.).
 * This version of the class uses @OneToMany.
 *
 * @author jules
 *
 */
public class User extends BaseEntity {
    public boolean isAccountNonExpired() {
        return true;
    }

    public boolean isAccountNonLocked() {
        return true;
    }

    public boolean isCredentialsNonExpired() {
        return true;
    }

    public boolean isEnabled() {
        return true;
    }

    private Set<Gift> gifts = new HashSet<Gift>();
    public Collection<Gift> getGifts() {
        return gifts;
    }

    public void setGifts(Set<Gift> gifts) {
        this.gifts = gifts;
    }

    public void addGift(Gift gift) {
        gifts.add(gift);
    }

    private Set<GiftChain> giftChains = new HashSet<GiftChain>();
    public Collection<GiftChain> getGiftChains() {
        return giftChains;
    }

    public void setGiftChains(Set<GiftChain> giftChains) {
        this.giftChains = giftChains;
    }

    public void addGiftChain(GiftChain chain) {
        giftChains.add(chain);
    }

    private Long touchedCount = 0L;

    public Long getTouchedCount() {
        return touchedCount;
    }

    public void setTouchedCount(Long touchedCount) {
        this.touchedCount = touchedCount;
    }

    private Long flaggedCount = 0L;

    public Long getFlaggedCount() {
        return flaggedCount;
    }

    public void setFlaggedCount(Long flaggedCount) {
        this.flaggedCount = flaggedCount;
    }

    private Boolean shouldHideFlagged = false;

    public Boolean getShouldHideFlagged() {
        return shouldHideFlagged;
    }

    public void setShouldHideFlagged(Boolean shouldHideFlagged) {
        this.shouldHideFlagged = shouldHideFlagged;
    }

    private String salt;

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    private String email;
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    private Short updateInterval = 60;

    public Short getUpdateInterval() {
        return updateInterval;
    }

    public void setUpdateInterval(Short updateInterval) {
        this.updateInterval = updateInterval;
    }

    // refers to a user gift
    private Long profilePicId;

    public Long getProfilePicId() {
        return profilePicId;
    }

    public void setProfilePicId(Long profilePicId) {
        this.profilePicId = profilePicId;
    }

    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int hashCode() {
        return Objects.hashCode(getId());
    }

    public boolean equals(Object other) {
        return getId() == ((User) other).getId();
    }
}
