package ken47.temp.droidbase2.models;

import com.google.common.base.Objects;

/**
 * Created by kenster on 11/27/14.
 */
public class Gift extends BaseEntity {
    private User user;
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    private GiftChain giftChain;
    public GiftChain getGiftChain() {
        return giftChain;
    }
    public void setGiftChain(GiftChain giftChain) {
        this.giftChain = giftChain;
    }

    private String username;
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isFlaggedByCurrentUser() {
        return flaggedByCurrentUser;
    }

    public void setFlaggedByCurrentUser(boolean flaggedByCurrentUser) {
        this.flaggedByCurrentUser = flaggedByCurrentUser;
    }

    public boolean isTouchedByCurrentUser() {
        return touchedByCurrentUser;
    }

    public void setTouchedByCurrentUser(boolean touchedByCurrentUser) {
        this.touchedByCurrentUser = touchedByCurrentUser;
    }

    private boolean flaggedByCurrentUser;
    private boolean touchedByCurrentUser;

    public String getGiftChainName() {
        return giftChainName;
    }

    public void setGiftChainName(String giftChainName) {
        this.giftChainName = giftChainName;
    }

    private String giftChainName;

    private String title;
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    private String imagePath;
    public String getImagePath() {
        return imagePath;
    }
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    private String caption;
    public String getCaption() {
        return caption;
    }
    public void setCaption(String caption) {
        this.caption = caption;
    }

    private Long viewCount = 0L;
    public Long getViewCount() {
        return viewCount;
    }
    public void setViewCount(Long viewCount) {
        this.viewCount = viewCount;
    }

    private Long touchedCount = 0L;
    public Long getTouchedCount() {
        return touchedCount;
    }
    public void setTouchedCount(Long touchedCount) {
        this.touchedCount = touchedCount;
    }

    private Long flaggedCount = 0L;
    public Long getFlaggedCount() {
        return flaggedCount;
    }
    public void setFlaggedCount(Long flaggedCount) {
        this.flaggedCount = flaggedCount;
    }

    @Override
    public boolean equals(final Object _other) {
        Gift other = (Gift) _other;
        return hashCode() == other.hashCode();
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getUser(), getTitle());
    }
}
