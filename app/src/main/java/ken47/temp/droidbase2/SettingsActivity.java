package ken47.temp.droidbase2;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.concurrent.Callable;

import ken47.temp.droidbase2.abstrakt.AuthorizedActivity;
import ken47.temp.droidbase2.helpers.CallableTask;
import ken47.temp.droidbase2.helpers.GiftApi;
import ken47.temp.droidbase2.helpers.SecuredRestBuilder;
import ken47.temp.droidbase2.helpers.TaskCallback;
import ken47.temp.droidbase2.helpers.UserState;
import ken47.temp.droidbase2.models.User;

public class SettingsActivity extends AuthorizedActivity {
    private SharedPreferences sharedPrefs;
    private CheckBox mShowFlaggedView;
    private RadioGroup mUpdateIntervalView;
    private RadioButton oneMinute, fiveMinutes, sixtyMinutes;
    private Button saveButton;
    private User user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setLayoutId(R.layout.settings_activity);
        super.onCreate(savedInstanceState);
        sharedPrefs = getApplicationContext().getSharedPreferences(AuthorizedActivity.APP_SHARED_PREFS, Context.MODE_PRIVATE);
        setViews();
        getUserInfo();
    }

    private void enableSaveButton() {
        saveButton.setText("Save");
        saveButton.setEnabled(true);
    }

    public void getUserInfo() {
        CallableTask.invoke(new Callable<User>() {

            @Override
            public User call() throws Exception {
                GiftApi api = SecuredRestBuilder.getRestAdapter(sharedPrefs).create(GiftApi.class);
                return api.getUserInfo();
            }
        }, new TaskCallback<User>() {

            @Override
            public void success(User user) {
                SettingsActivity.this.user = user;
                enableSaveButton();
            }

            @Override
            public void error(Exception e) {
                Log.e(LoginActivity.class.getName(), "Error loading user info.", e);

                Toast.makeText(
                        SettingsActivity.this,
                        "Load error! Try again in a minute.",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void always() {}
        });
    }

    private void setViews() {
        mShowFlaggedView = (CheckBox)findViewById(R.id.showFlaggedView);
        mUpdateIntervalView = (RadioGroup)findViewById(R.id.updateIntervalView);
        oneMinute = (RadioButton)findViewById(R.id.oneMinute);
        fiveMinutes = (RadioButton)findViewById(R.id.fiveMinutes);
        sixtyMinutes = (RadioButton)findViewById(R.id.sixtyMinutes);
        saveButton = (Button)findViewById(R.id.saveButton);

        mShowFlaggedView.setChecked(sharedPrefs.getBoolean(UserState.SHOULD_HIDE_FLAGGED, false));
        int updateInterval = sharedPrefs.getInt(UserState.UPDATE_INTERVAL, 60);

        if (updateInterval == 1) {
            oneMinute.setChecked(true);
        } else if (updateInterval == 5) {
            fiveMinutes.setChecked(true);
        } else if (updateInterval == 60) {
            sixtyMinutes.setChecked(true);
        }
    }

    public void attemptSave(View view) {
        CallableTask.invoke(new Callable<Void>() {

            @Override
            public Void call() throws Exception {
                GiftApi api = SecuredRestBuilder.getRestAdapter(sharedPrefs).create(GiftApi.class);
                user.setShouldHideFlagged(mShowFlaggedView.isChecked());
                int selectedId = mUpdateIntervalView.getCheckedRadioButtonId();

                // find which radioButton is checked by id
                if(selectedId == oneMinute.getId()) {
                    user.setUpdateInterval((short)1);
                } else if(selectedId == fiveMinutes.getId()) {
                    user.setUpdateInterval((short)5);
                } else if(selectedId == sixtyMinutes.getId()) {
                    user.setUpdateInterval((short)60);
                }

                // there's def a better way to do this than loading + sending a user object.
                // this is subject to race conditions.
                api.persistSettings(user);
                return null;
            }
        }, new TaskCallback<Void>() {

            @Override
            public void success(Void gift) {
                Toast.makeText(
                        SettingsActivity.this,
                        "Save success!",
                        Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putInt(UserState.UPDATE_INTERVAL, user.getUpdateInterval());
                editor.putBoolean(UserState.SHOULD_HIDE_FLAGGED, user.getShouldHideFlagged());
                editor.commit();
            }

            @Override
            public void error(Exception e) {
                Toast.makeText(
                        SettingsActivity.this,
                        "Save failed!",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void always() {
            }
        });
    }
}
