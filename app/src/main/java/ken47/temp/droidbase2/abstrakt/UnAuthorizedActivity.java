package ken47.temp.droidbase2.abstrakt;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import ken47.temp.droidbase2.GiftListActivity;
import ken47.temp.droidbase2.helpers.UserState;

public abstract class UnAuthorizedActivity extends Activity {
    protected void redirectIfLoggedIn() {
        SharedPreferences sharedPrefs = getApplicationContext().getSharedPreferences(AuthorizedActivity.APP_SHARED_PREFS, Context.MODE_PRIVATE);
        if (UserState.isProbablyLoggedIn(sharedPrefs)) {
            Intent intent = new Intent(this, GiftListActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onResume() {
        redirectIfLoggedIn();
        super.onResume();
    }

    @Override
    protected void onRestart() {
        redirectIfLoggedIn();
        super.onRestart();
    }

    @Override
    // see http://stackoverflow.com/questions/17915393/android-login-activity-and-home-activity-redirection
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        redirectIfLoggedIn();
    }
}
