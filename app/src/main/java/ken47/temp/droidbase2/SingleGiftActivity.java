package ken47.temp.droidbase2;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;
import java.util.concurrent.Callable;

import ken47.temp.droidbase2.abstrakt.AuthorizedActivity;
import ken47.temp.droidbase2.helpers.CallableTask;
import ken47.temp.droidbase2.helpers.GiftApi;
import ken47.temp.droidbase2.helpers.SecuredRestBuilder;
import ken47.temp.droidbase2.helpers.TaskCallback;
import ken47.temp.droidbase2.helpers.UserState;
import ken47.temp.droidbase2.models.Gift;

public class SingleGiftActivity extends AuthorizedActivity {
    public static final String GIFT_ID = "gift_id";

    private Long giftId;
    private final String INTENT_NAME = "ken47.temp.droidbase2.SingleGiftActivity.AlarmReceiver";
    private final AlarmReceiver mReceiver = new AlarmReceiver();
    private SharedPreferences sharedPrefs;

    private ImageView mImageView;
    private TextView mTitleView, mCaptionView, mFlaggedCountView, mTouchedCountView, mUsernameView;
    private Button mTouchButtonView, mFlagButtonView;

    private Gift gift;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setLayoutId(R.layout.single_gift_activity);
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            giftId = savedInstanceState.getLong(GIFT_ID);
        } else {
            Bundle b = getIntent().getExtras();
            giftId = b.getLong(GIFT_ID);
        }

        assert(giftId != null);

        sharedPrefs = getApplicationContext().getSharedPreferences(AuthorizedActivity.APP_SHARED_PREFS, Context.MODE_PRIVATE);
        setViews();
    }

    @Override
    public void onStart() {
        setBroadcastReceiver();
        setAlarm();
        loadGift();
        super.onStart();
    }

    private void setBroadcastReceiver() {
        IntentFilter intentFilter = new IntentFilter(INTENT_NAME);
        registerReceiver(mReceiver, intentFilter);
    }

    private void setViews() {
        mImageView = (ImageView)findViewById(R.id.imageView);
        mTitleView = (TextView)findViewById(R.id.titleView);
        mCaptionView = (TextView)findViewById(R.id.captionView);
        mTouchedCountView = (TextView)findViewById(R.id.touchedCountView);
        mFlaggedCountView = (TextView)findViewById(R.id.flaggedCountView);
        mUsernameView = (TextView)findViewById(R.id.usernameView);

        mTouchButtonView = (Button)findViewById(R.id.touchButton);
        mFlagButtonView = (Button)findViewById(R.id.flagButton);
    }

    @Override
    public void onStop() {
        unregisterReceiver(mReceiver);
        super.onStop();
    }

    public void setAlarm() {
        Context context = getApplicationContext();
        Intent intent = new Intent(INTENT_NAME);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

        AlarmManager alarm = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        alarm.set(
                AlarmManager.RTC,
                System.currentTimeMillis() + 15000, // TODO: System.currentTimeMillis() + sharedPrefs.getLong(UserState.UPDATE_INTERVAL, 60L) * 1000 * 60,
                pendingIntent
        );
    }

    public void redrawActivityUI() {
        // user is allowed to view his own image, even if it is flagged, regardless of his setting
        if (sharedPrefs.getString(UserState.USERNAME, "").equals(gift.getUsername())) {
            mFlaggedCountView.setText("Flagged x" + gift.getFlaggedCount());
            mFlaggedCountView.setVisibility(View.VISIBLE);
        } else {
            if (sharedPrefs.getBoolean(UserState.SHOULD_HIDE_FLAGGED, false) && gift.getFlaggedCount() > 0) {
                Toast.makeText(
                        SingleGiftActivity.this,
                        "This gift has been flagged and you indicated that you do not want to see flagged images.",
                        Toast.LENGTH_SHORT).show();

                return;
            }

            mFlagButtonView.setVisibility(View.VISIBLE);
            mTouchButtonView.setVisibility(View.VISIBLE);



            if (gift.isTouchedByCurrentUser()) {
                mTouchButtonView.setText("I'm Not Touched");
            } else {
                mTouchButtonView.setText("I'm Touched");
            }

            if (gift.isFlaggedByCurrentUser()) {
                mFlagButtonView.setText("Unflag");
            } else {
                mFlagButtonView.setText("Flag");
            }
        }

        StringBuilder sb = new StringBuilder();
        sb.append(gift.getUsername());
        String giftChainName = gift.getGiftChainName();
        if (null != giftChainName && !giftChainName.isEmpty()) {
            sb.append(" / " + gift.getGiftChainName());
        }

        mUsernameView.setText(sb.toString());
        mTitleView.setText(gift.getTitle());

        if (gift.getCaption() != null) {
            mCaptionView.setText(gift.getCaption());
            mCaptionView.setTypeface(null, Typeface.ITALIC);
            mCaptionView.setVisibility(View.VISIBLE);
        }

        mTouchedCountView.setText("Touched x" + gift.getTouchedCount());

        Toast.makeText(getApplicationContext(), "Touch count updated.", Toast.LENGTH_LONG).show();
        loadImagePicture();
    }

    public void loadImagePicture() {
        CallableTask.invoke(new Callable<Bitmap>() {

            @Override
            public Bitmap call() throws Exception {
                // TODO: constant
                String url = SecuredRestBuilder.STATIC_SERVER + "/" + gift.getImagePath();
                Bitmap mIcon11 = null;
                InputStream in = new java.net.URL(url).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
                return mIcon11;
            }
        }, new TaskCallback<Bitmap>() {

            @Override
            public void success(Bitmap result) {
                mImageView.setImageBitmap(result);
            }

            @Override
            public void error(Exception e) {
                Log.e(SingleGiftActivity.class.getName(), "Error loading gift image.", e);

                Toast.makeText(
                        SingleGiftActivity.this,
                        "Image load failed, check your Internet connection and credentials.",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void always() {}
        });
    }

    private boolean flagLocked = false;

    public void attemptToggleFlagged(View view) {
        if (flagLocked) return;
        flagLocked = true;
        mFlagButtonView.setEnabled(false);

        CallableTask.invoke(new Callable<Void>() {

            @Override
            public Void call() throws Exception {
                GiftApi api = SecuredRestBuilder.getRestAdapter(sharedPrefs).create(GiftApi.class);

                if (gift.isFlaggedByCurrentUser()) {
                    api.unFlagGift(giftId);
                } else {
                    api.flagGift(giftId);
                }

                return null;
            }
        }, new TaskCallback<Void>() {

            @Override
            public void success(Void v) {
                if (gift.isFlaggedByCurrentUser()) {
                    gift.setFlaggedCount(gift.getFlaggedCount() - 1);
                    gift.setFlaggedByCurrentUser(false);
                } else {
                    gift.setFlaggedCount(gift.getFlaggedCount() + 1);
                    gift.setFlaggedByCurrentUser(true);
                }
                redrawActivityUI();
            }

            @Override
            public void error(Exception e) {
                Log.e(SingleGiftActivity.class.getName(), "Error toggling touched status.", e);

                Toast.makeText(
                        SingleGiftActivity.this,
                        "There was an error with this operation.",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void always() {
                flagLocked = false;
                mFlagButtonView.setEnabled(true);
            }
        });
    }

    private boolean touchLocked = false;

    public void attemptToggleTouched(View view) {
        if (touchLocked) return;
        touchLocked = true;
        mTouchButtonView.setEnabled(false);

        CallableTask.invoke(new Callable<Void>() {

            @Override
            public Void call() throws Exception {
                GiftApi api = SecuredRestBuilder.getRestAdapter(sharedPrefs).create(GiftApi.class);

                if (gift.isTouchedByCurrentUser()) {
                    api.unTouchGift(giftId);
                } else {
                    api.touchGift(giftId);
                }

                return null;
            }
        }, new TaskCallback<Void>() {

            @Override
            public void success(Void v) {
                if (gift.isTouchedByCurrentUser()) {
                    gift.setTouchedCount(gift.getTouchedCount() - 1);
                    gift.setTouchedByCurrentUser(false);
                } else {
                    gift.setTouchedCount(gift.getTouchedCount() + 1);
                    gift.setTouchedByCurrentUser(true);
                }
                redrawActivityUI();
            }

            @Override
            public void error(Exception e) {
                Log.e(SingleGiftActivity.class.getName(), "Error toggling touched status.", e);

                Toast.makeText(
                        SingleGiftActivity.this,
                        "There was an error with this operation.",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void always() {
                touchLocked = false;
                mTouchButtonView.setEnabled(true);
            }
        });
    }

    public void loadGift() {
        CallableTask.invoke(new Callable<Gift>() {

            @Override
            public Gift call() throws Exception {
                GiftApi loginApi = SecuredRestBuilder.getRestAdapter(sharedPrefs).create(GiftApi.class);
                Gift response = loginApi.loadGift(giftId);

                // TODO: this APP_SHARED_PREFS thing def belongs in its own config
                return response;
            }
        }, new TaskCallback<Gift>() {

            @Override
            public void success(Gift gift) {
                SingleGiftActivity.this.gift = gift;
                redrawActivityUI();
                setAlarm();
            }

            @Override
            public void error(Exception e) {
                Log.e(SingleGiftActivity.class.getName(), "Error loading gift.", e);

                Toast.makeText(
                        SingleGiftActivity.this,
                        "Load failed, check your Internet connection and credentials.",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void always() {}
        });
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        savedInstanceState.putLong(GIFT_ID, giftId);

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    private class AlarmReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            Log.d("DEBUG", "ALARM TRIGGERED");
            // call method that reloads the image
            loadGift();
        }
    }
}
