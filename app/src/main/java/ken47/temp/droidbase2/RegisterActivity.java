package ken47.temp.droidbase2;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.concurrent.Callable;

import ken47.temp.droidbase2.abstrakt.AuthorizedActivity;
import ken47.temp.droidbase2.abstrakt.UnAuthorizedActivity;
import ken47.temp.droidbase2.helpers.CallableTask;
import ken47.temp.droidbase2.helpers.GiftApi;
import ken47.temp.droidbase2.helpers.SecuredRestBuilder;
import ken47.temp.droidbase2.helpers.TaskCallback;
import ken47.temp.droidbase2.models.User;

public class RegisterActivity extends UnAuthorizedActivity {
    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.register);
    }

    public void attemptRegister(View view) {
        final String username = ((EditText)findViewById(R.id.registerUsername)).getText().toString();
        final String email = ((EditText)findViewById(R.id.registerEmail)).getText().toString();
        final String password = ((EditText)findViewById(R.id.registerPassword)).getText().toString();
        final String repeatPassword = ((EditText)findViewById(R.id.registerRepeatPassword)).getText().toString();

        if (!password.equals(repeatPassword)) {
            Toast.makeText(
                    RegisterActivity.this,
                    "Your passwords do not match.",
                    Toast.LENGTH_SHORT).show();
        }

        final User user = new User();
        user.setUsername(username);
        user.setEmail(email);
        user.setPassword(password);


        CallableTask.invoke(new Callable<Boolean>() {

            @Override
            public Boolean call() throws Exception {
                GiftApi api = SecuredRestBuilder.getRestAdapterForRegistration().create(GiftApi.class);
                Boolean response = api.register(user);
                return response;
            }
        }, new TaskCallback<Boolean>() {

            @Override
            public void success(Boolean result) {
                // OAuth 2.0 grant was successful and we
                // can talk to the server, open up the video listing
                startActivity(new Intent(
                        RegisterActivity.this,
                        GiftListActivity.class));
            }

            @Override
            public void error(Exception e) {
                Log.e(RegisterActivity.class.getName(), "Error logging in via OAuth.", e);

                Toast.makeText(
                        RegisterActivity.this,
                        "Login failed, check your Internet connection and credentials.",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void always() {
            }
        });
    }
}
