package ken47.temp.droidbase2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import ken47.temp.droidbase2.abstrakt.AuthorizedActivity;
import ken47.temp.droidbase2.helpers.CallableTask;
import ken47.temp.droidbase2.helpers.GiftApi;
import ken47.temp.droidbase2.helpers.SecuredRestBuilder;
import ken47.temp.droidbase2.helpers.TaskCallback;
import ken47.temp.droidbase2.helpers.UserState;
import ken47.temp.droidbase2.models.GiftChain;

public class PickGiftChainActivity extends AuthorizedActivity {
    private SharedPreferences mSharedPrefs;
    private EditText mGiftChainNameInput;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setLayoutId(R.layout.add_gift_chain_activity);
        super.onCreate(savedInstanceState);
        mSharedPrefs = getApplicationContext().getSharedPreferences(AuthorizedActivity.APP_SHARED_PREFS, Context.MODE_PRIVATE);
        mGiftChainNameInput = (EditText) findViewById(R.id.giftChainNameView);


        loadMyGiftChains();
    }

    private void displayGiftChains(final List<GiftChain> chains) {
        // We get the ListView component from the layout
        ListView lv = (ListView) findViewById(R.id.listView);
        final List<Map<String, String>> sourceList = new ArrayList<Map<String, String>>();

        for(GiftChain chain : chains) {
            HashMap<String, String> tmp = new HashMap<String, String>();
            tmp.put("name", chain.getName());
            sourceList.add(tmp);
        }

        // This is a simple adapter that accepts as parameter
        // Context
        // Data list
        // The row layout that is used during the row creation
        // The keys used to retrieve the data
        // The View id used to show the data. The key number and the view id must match
        SimpleAdapter simpleAdapter = new SimpleAdapter(this, sourceList, R.layout.pick_chain_item_view, new String[] {"name"}, new int[] {R.id.nameText});
        lv.setAdapter(simpleAdapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parentAdapter, View view, int position, long id) {

                // We know the View is a TextView so we can cast it
                Map<String, String> item = sourceList.get(position);
                Intent intent = new Intent(PickGiftChainActivity.this, AddGiftActivity.class);
                Bundle b = new Bundle();
                // TODO: change
                b.putLong("chain_id", chains.get(position).getId()); //Your id
                intent.putExtras(b);
                startActivity(intent);
            }
        });
    }

    public void attemptCreateGiftChain(View view) {
        CallableTask.invoke(new Callable<GiftChain>() {

            @Override
            public GiftChain call() throws Exception {
                GiftApi api = SecuredRestBuilder.getRestAdapter(mSharedPrefs).create(GiftApi.class);
                String name = mGiftChainNameInput.getText().toString();

                if (name.isEmpty()) {
                    throw new Exception("Name must be non-empty.");
                }
                return api.createGiftChain(name);
            }
        }, new TaskCallback<GiftChain>() {

            @Override
            public void success(GiftChain chain) {
                Log.d("DEBUG", "gift chain made: " + chain.getName());
                Intent intent = new Intent(PickGiftChainActivity.this, AddGiftActivity.class);
                Bundle b = new Bundle();
                // TODO: change
                b.putLong("chain_id", chain.getId()); //Your id
                intent.putExtras(b);
                startActivity(intent);
            }

            @Override
            public void error(Exception e) {
                Log.e(SingleGiftActivity.class.getName(), "Error toggling touched status.", e);

                Toast.makeText(
                        PickGiftChainActivity.this,
                        "There was an error with this operation.",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void always() {
            }
        });
    }

    private void loadMyGiftChains() {
        CallableTask.invoke(new Callable<List<GiftChain>>() {
            @Override
            public List<GiftChain> call() throws Exception {
                GiftApi api = SecuredRestBuilder.getRestAdapter(mSharedPrefs).create(GiftApi.class);

                return api.getUserGiftChains(mSharedPrefs.getString(UserState.USERNAME, ""));
            }
        }, new TaskCallback<List<GiftChain>>() {

            @Override
            public void success(List<GiftChain> chains) {
                displayGiftChains(chains);
            }

            @Override
            public void error(Exception e) {
                Log.e(LoginActivity.class.getName(), "Error loading user gift chains.", e);

                Toast.makeText(
                        PickGiftChainActivity.this,
                        "Load error! Try again in a minute.",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void always() {
            }
        });
    }
}
